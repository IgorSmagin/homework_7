package com.company.bank;


import java.sql.Date;

public class Client {
    private String firstName;
    private String lastName;
    private Date dateOfBirth;
    private String address;
    private String phone;

    public Client(String firstName,String lastName,Date dateOfBirth,String address,String phone ){
        this.firstName=firstName;
        this.lastName=lastName;
        this.dateOfBirth=dateOfBirth;
        this.address=address;
        this.phone=phone;
    }

    public String getFirstName(){return firstName;}
    public String getLastName(){return lastName;}
    public Date getDateOfBirth(){return dateOfBirth;}
    public String getAddress(){return address;}
    public String getPhone(){return phone;}
    public void setFirstName(String firstName){this.firstName=firstName;}
    public void setLastName(String lastName){this.lastName=lastName;}
    public void setDateOfBirth(Date dateOfBirth){this.dateOfBirth=dateOfBirth;}
    public void setAddress(String address){this.address=address;}
    public void setPhone(String phone){this.phone=phone;}

    @Override
    public boolean equals(Object o){
        if (this==o) return true;
        if (!(o instanceof Client)) return false;

        Client client = (Client) o;

        if (!firstName.equals(client.firstName)) return false;
        if (!lastName.equals(client.lastName)) return  false;
        if (!dateOfBirth.equals(client.dateOfBirth)) return false;
        return address==client.address;

    }

    @Override
    public int hashCode(){
        int result = firstName.hashCode();
        result = 31*result+lastName.hashCode();
        result = 31*result+dateOfBirth.hashCode();
        result = 31*result+address.hashCode();
        return result;
    }

    @Override
    public String toString(){
        final StringBuffer sb = new StringBuffer("Client{");
        sb.append("First name ").append(firstName);
        sb.append("Last name ").append (lastName);
        sb.append("Date of birth: ").append(dateOfBirth);
        sb.append("Adress: ").append(address);
        sb.append("Phone: ").append(phone);
        sb.append(" }");
        return sb.toString();
    }


}
